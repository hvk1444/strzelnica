## About 

This is a simple web application, which allows users to register as customers of a shooting range, create appointments at the range and manage them. The application was created as a project for the university course.

## Functionalities

The application offers basic CRUD functionalities - registered users can Create, Read, Update and Delete their appointments. It also contains an admin panel - users designated as administrators can view all registered users and their appointments, and delete them if necessary.

## Technologies

The application is written in PHP with [Laravel 8](https://laravel.com/) framework, and it works with MySQL database. The frontend is created mostly with HTML5 and CSS3, and it makes use of Laravel's Blade templates. The application is developed and tested with [XAMPP](https://www.apachefriends.org/index.html). 

