<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeaponsReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weapons_reservations', function (Blueprint $table) {
            $table->unsignedBigInteger('appointment_id');
            $table->unsignedBigInteger('weapon_id');

            $table->primary(['appointment_id', 'weapon_id']);
            $table->foreign('appointment_id')->references('id')->on('appointments')
                ->onUpdate('cascade')->onDelete('cascade');
            $table->foreign('weapon_id')->references('id')->on('weapons')
                ->onUpdate('cascade')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weapons_reservations');
    }
}
