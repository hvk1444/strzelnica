<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AppointmentsController;
use App\Http\Controllers\AdminController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Auth::routes();
//home
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//appointments
Route::get('/appointments', [AppointmentsController::class, 'index'])->name('appointments');
Route::get('/appointments/create', [AppointmentsController::class, 'create'])->name('create');
Route::post('/appointments/create', [AppointmentsController::class, 'store'])->name('store');
Route::get('/appointments/edit/{id}', [AppointmentsController::class, 'edit'])->name('edit');
Route::put('/appointments/{id}', [AppointmentsController::class, 'update'])->name('update');
Route::get('/appointments/delete/{id}',[AppointmentsController::class,'destroy'])->name('delete');
//admin
Route::get('/admin', [AdminController::class, 'index'])->name('admin');
Route::get('/admin/deleteAppointment/{id}', [AdminController::class,'destroyAppointment'])->name('adminDeleteAppointment');
Route::get('/admin/deleteUser/{id}', [AdminController::class, 'destroyUser'])->name('adminDeleteUser');
