@extends('layouts.app')

@section('title')
    <title>Strzelnica | Strona główna</title>
@endsection

@section('styles')
    <link href="{{ asset('css\welcome.css') }}" rel="stylesheet">
@endsection

@section('content')
    <header>
        <h1>Witaj na stronie naszej strzelnicy!</h1>
    </header>
    <section>
        <p>Na naszej stronie możesz bezpłatnie zapisać się z wyprzedzeniem na wizytę w naszej strzelnicy. Dzięki temu możesz mieć pewność, że w umówionym czasie na obiekcie będzie instruktor gotowy Cię obsłużyć, a wybrane przez Ciebie broń i amunicja będą przygotowane na wolnym stanowisku.</p>
    </section>
@endsection
