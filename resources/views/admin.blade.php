@extends('layouts.app')

@section('title')
    <title>Strzelnica | Panel administracyjny</title>
@endsection

@section('styles')
    <link href="{{ asset('css\admin.css') }}" rel="stylesheet">
@endsection

@section('content')
    @auth
        <header>
            <h1>
                Panel administracyjny
            </h1>
            <p>
                Jako administrator możesz usuwać użytkowników i ich rezerwacje.
            </p>
        </header>
        <section>
            <div class="table-container">
                @if ($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif
                <h1>Użytkownicy</h1>
                <table data-toggle=table">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>E-mail</th>
                        <th>Opcje</th>
                    </tr>
                    </thead>
                    <thead>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->email}}</td>
                            <td>
                                <a href="{{ route('adminDeleteUser', $user->id) }}"
                                   class="btn btn-danger"
                                   onclick="return confirm('Usunięcie użytkownika jest bezpowrotne. Czy jesteś pewien, że chcesz usunąć użytkownika?')"
                                   title="Skasuj"> Usuń
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </thead>
                </table>
                <h1>Rezerwacje</h1>
                <table data-toggle="table">
                    <thead>
                    <tr>
                        <th>ID rezerwacji</th>
                        <th>Użytkownik</th>
                        <th>Data i godzina rezerwacji</th>
                        <th>Wybrane modele broni</th>
                        <th>Opcje</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($appointments as $appointment)
                        <tr>
                            <td>{{$appointment->id}}</td>
                            <td>{{$appointment->user->email}}</td>
                            <td>{{$appointment->appointment_time}}</td>
                            <td><ul>
                                    @foreach($appointment->reserved_weapons as $reserved_weapon)
                                        <li>{{$reserved_weapon->name}}</li>
                                    @endforeach
                                </ul></td>
                            <td>
                                <a href="{{ route('adminDeleteAppointment', $appointment->id) }}"
                                   class="btn btn-danger"
                                   onclick="return confirm('Usunięcie rezerwacji jest bezpowrotne. Czy jesteś pewien, że chcesz usunąć rezerwację?')"
                                   title="Skasuj"> Usuń
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="footer-button">
                    <a href="{{route('create')}}" class="btn btn-secondary">Dodaj</a>
                </div>
            </div>
        </section>
    @endauth

    @guest
        <header>
            Nie powinno cię tu być.
        </header>
    @endguest
@endsection
