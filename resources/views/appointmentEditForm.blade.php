@extends('layouts.app')

@section('title')
    <title>Strzelnica | Modyfikuj rezerwację</title>
@endsection

@section('styles')
    <link href="{{ asset('css\appointmentsForm.css') }}" rel="stylesheet">
@endsection

@section('content')
    <header>
        <h1>
            Modyfikuj rezerwację
        </h1>
    </header>
    <section>
        <p>
            Obecny stan rezerwacji:
            {{ $appointment->appointment_time }}
            @foreach($appointment->reserved_weapons as $reserved_weapon)
                <li>{{$reserved_weapon->name}}</li>
            @endforeach
        </p>
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
        <form role="form" id="appointment-form" method="POST" action="{{ route('update', $appointment) }}">
            @method('PUT')
            @csrf
            <div class="box">
                <div class="box-body">
                    <div class="form-group{{ $errors->has('message') ? ' has-error' : '' }}"
                         id="roles_box">
                        <label><b>Data i czas rezerwacji (miesiące luty - grudzień 2021)</b></label> <br>
                        <input type="datetime-local" name="appointment_time" id="appointment_time"
                               required min="2021-02-01T00:00" max="2021-12-31T23:59" value="{{ $appointment->appointment_time }}">
                        <p>Jeśli twoja przeglądarka nie wyświetla opcji wyboru daty z kalendarza, prosimy o wpisanie daty w następującym formacie:
                            <br/>RRRR-MM-DDTgg:mm <br/> gdzie: RRRR - rok, MM - miesiąc, DD - dzień, gg - godzina, mm - minuta, T - separator (przepisać tak jak we wzorze)</p>
                        <label><b>Dostępne modele broni:</b></label> <br>
                        <table>
                            @foreach($weapons as $weapon)
                                <tr>
                                    <td><label for="{{$weapon->id}}">{{$weapon->name}}</label></td>
                                    <td><input type="checkbox" id="{{$weapon->id}}" name="weapons[]" value="{{$weapon->id}}"></td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </div>
            <div class="box-footer">
                <button type="submit" class="btn btn-success">Zapisz</button>
            </div>
        </form>
    </section>
@endsection
