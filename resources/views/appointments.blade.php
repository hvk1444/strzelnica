@extends('layouts.app')

@section('title')
    <title>Strzelnica | Rezerwacje</title>
@endsection

@section('styles')
    <link href="{{ asset('css\appointments.css') }}" rel="stylesheet">
@endsection

@section('content')
    @auth
        <header>
            <h1>
                Utwórz, wyświetl, zmień lub usuń rezerwację
            </h1>
        </header>
        <section>
            <table data-toggle="table">
                <thead>
                <tr>
                    <th>ID rezerwacji</th>
                    <th>Data i godzina rezerwacji</th>
                    <th>Wybrane modele broni</th>
                    <th>Opcje</th>
                </tr>
                </thead>
                <tbody>
                @foreach($appointments as $appointment)
                    <tr>
                        <td>{{$appointment->id}}</td>
                        <td>{{$appointment->appointment_time}}</td>
                        <td><ul>
                            @foreach($appointment->reserved_weapons as $reserved_weapon)
                                <li>{{$reserved_weapon->name}}</li>
                            @endforeach
                            </ul></td>
                        <td>
                            <a href="{{ route('edit', $appointment) }}"
                               class="btn btn-success"
                               title="Edytuj"> Edytuj
                            </a>
                            <a href="{{ route('delete', $appointment->id) }}"
                               class="btn btn-danger"
                               onclick="return confirm('Jesteś pewien?')"
                               title="Skasuj"> Usuń
                            </a>
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="footer-button">
                <a href="{{route('create')}}" class="btn btn-secondary">Dodaj</a>
            </div>
        </section>
    @endauth

    @guest
        <header>
            Zaloguj się aby przejrzeć swoje rezerwacje.
        </header>
    @endguest
@endsection
