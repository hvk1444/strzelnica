<?php

return [

    'Login' => 'Logowanie',
    'Log in' => 'Zaloguj',
    'E-Mail Address' => 'Adres e-mail',
    'Password' => 'Hasło',
    'Forgot Your Password?' => 'Resetuj hasło',
    'Remember Me' => 'Zapamiętaj mnie',
    'Register' => 'Rejestracja',
    'Confirm Password' => 'Powtórz hasło',
    'Register Me' => 'Zarejestruj',
    'Logout' => "Wyloguj",
    'You are logged in!' => "Pomyślnie zalogowano!"
];
