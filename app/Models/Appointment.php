<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;

    /**
     * @var mixed
     */
    private $user_id;
    /**
     * @var mixed
     */
    private $appointment_time;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function weapon()
    {
        return $this->belongsToMany(Weapon::class)->withTimestamps();
    }

}
