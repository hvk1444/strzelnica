<?php

namespace App\Http\Controllers;


use App\Models\Appointment;
use App\Models\User;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Auth;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(!Gate::allows('admin'))
        {
            return('Próba nieautoryzowanego dostępu.');
        }
        $users = User::orderBy('id','asc')->get();
        $appointments = Appointment::orderBy('id', 'asc')->get();
        //add info about reserved weapons to each appointment object - refactor this to query DB once
        foreach($appointments as $appointment)
        {
            /*
            array_push($reserved_weapons,
                DB::table('weapons')
                    ->join('weapon_reservations', 'weapons.id', '=', 'weapon_reservations.weapon_id')
                    ->join('appointments', 'weapon_reservations.appointment_id', '=', 'appointments.id')
                    ->select('weapons.name')
                    ->where('appointments.user_id', '=', Auth::id())
                    ->get()
            );
            */
            $appointment->reserved_weapons =
                DB::table('weapons')
                    ->join('appointment_weapon', 'weapons.id', '=', 'appointment_weapon.weapon_id')
                    ->select('weapons.name')
                    ->where('appointment_weapon.appointment_id', '=', $appointment->id)
                    ->get();
        }
        return view('admin', ['appointments' => $appointments, 'users' => $users]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove specified appointment from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAppointment($id)
    {
        if(!Gate::allows('admin'))
        {
            return('Próba nieautoryzowanego dostępu.');
        }
        if(Appointment::destroy($id)){

            return redirect()->route('admin');
        }
        return redirect()->route('admin')
            ->withErrors(['Wystąpił niezidentyfikowany błąd - nie udało się usunąć .']);
    }

    /**
     * Remove specified user from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyUser($id)
    {
        if(!Gate::allows('admin'))
        {
            return('Próba nieautoryzowanego dostępu.');
        }
        if($id == Auth::id())
        {
            return redirect()->route('admin')
                ->withErrors(["Nie możesz usunąć użytkownika, który jest obecnie zalogowany."]);
        }
        if(User::destroy($id)){

            return redirect()->route('admin');
        }
        return redirect()->route('admin')
            ->withErrors(['Wystąpił niezidentyfikowany błąd - nie udało się usunąć.']);
    }
}
