<?php

namespace App\Http\Controllers;


use App\Models\Appointment;
use App\Models\Weapon;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AppointmentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user() != null)
        {
            $appointments = Appointment::where('user_id', Auth::id())->get();
            //add info about reserved weapons to each appointment object - refactor this to query DB once
            foreach($appointments as $appointment)
            {
                /*
                array_push($reserved_weapons,
                    DB::table('weapons')
                        ->join('weapon_reservations', 'weapons.id', '=', 'weapon_reservations.weapon_id')
                        ->join('appointments', 'weapon_reservations.appointment_id', '=', 'appointments.id')
                        ->select('weapons.name')
                        ->where('appointments.user_id', '=', Auth::id())
                        ->get()
                );
                */
                $appointment->reserved_weapons =
                    DB::table('weapons')
                    ->join('appointment_weapon', 'weapons.id', '=', 'appointment_weapon.weapon_id')
                    ->select('weapons.name')
                    ->where('appointment_weapon.appointment_id', '=', $appointment->id)
                    ->get();
            }
            return view('appointments', ['appointments' => $appointments]);
        }
        return view('appointments');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $appointment = new Appointment();
        $weapons = Weapon::orderBy('id','asc')->get();
        return view ('appointmentsForm',['appointment' => $appointment, 'weapons' => $weapons]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'appointment_time' => 'bail|required|date_format:"Y-m-d\TH:i"|before:"31 December 2021"|after:"1 February 2021"',
            'weapons' => 'required|filled'
        ]);
        //if validation fails, redirect back to the form creator
        if ($validator->fails()) {
            return redirect('appointments/create')
                ->withErrors($validator)
                ->withInput();
        }
        //if there is no user logged in, redirect to homepage
        if(Auth::user() == null)
        {
            return view('home');
        }
        //define data to be saved into the DB
        $appointment = new Appointment();   //create new Appointment model
        $appointment->user_id = Auth::id();  //get current user ID
        $appointment->appointment_time = $request->input('appointment_time'); //get datetime array from validated input
        if($appointment->save())
        {
            $appointment = Appointment::orderBy('id','desc')->first(); //get the latest appointment in the database
            $weapons = $request->input('weapons'); //get array of checkbox values from validated input
            $appointment->weapon()->attach($weapons); //attach all the array values to the latest appointment
            return redirect('appointments');
        }

        return view('appointments');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //query DB for appointment with given ID
        $appointment = Appointment::find($id);
        //check if current user is the appointment's author
        if (Auth::id() != $appointment->user_id) {

            return back()->with(['success' => false, 'message_type' => 'danger',
                'message' => 'Nie posiadasz uprawnień do przeprowadzenia tej operacji.']);

        }
        //query DB for reserved weapons
        $appointment->reserved_weapons =
            DB::table('weapons')
            ->join('appointment_weapon', 'weapons.id', '=', 'appointment_weapon.weapon_id')
            ->select('weapons.name')
            ->where('appointment_weapon.appointment_id', '=', $appointment->id)
            ->get();
        //query DB for all weapon IDs
        $weapons = Weapon::orderBy('id','asc')->get();
        return view('appointmentEditForm', ['appointment'=>$appointment, 'weapons' => $weapons]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //define validation rules
        $validator = Validator::make($request->all(), [
            'appointment_time' => 'bail|required|date_format:"Y-m-d\TH:i"|before:"31 December 2021"|after:"1 February 2021"',
            'weapons' => 'required|filled'
        ]);
        //if validation fails, redirect back to the form editor
        if ($validator->fails()) {
            return redirect("appointments/edit/$id")
                ->withErrors($validator)
                ->withInput();
        }
        $appointment = Appointment::find($id);
        //check if current user is the appointment's owner
        if(Auth::id() != $appointment->user_id)
        {
            return back()->with(['success' => false, 'message_type' => 'danger',
                'message' => 'Nie posiadasz uprawnień do przeprowadzenia tej operacji.']);
        }
        $appointment->appointment_time = $request->appointment_time;
        if($appointment->save())
        {
            $weapons = $request->input('weapons'); //get array of checkbox values from validated input
            $appointment->weapon()->sync($weapons); //attach all the array values to the latest appointment
            return redirect()->route('appointments');
        }
        return "Wystąpił błąd.";
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $appointment = Appointment::find($id);
        if(Auth::id() != $appointment->user_id)
        {
            return back()->with(['success' => false, 'message_type' => 'danger',
                'message' => 'Nie posiadasz uprawnień do przeprowadzenia tej operacji.']);
        }
        if($appointment->delete()){

            return redirect()->route('appointments');
        }
        return "Wystąpił błąd.";

    }
}
